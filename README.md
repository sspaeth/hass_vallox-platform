Vallox platform for Home Assistant
==================================

Requirements
------------

 - Python 3
 - vallox_websocket_api (https://pypi.org/project/vallox-websocket-api/)

License
-------

In line with home-assistant itself this component is licensed under
the Apache License 2.0. See LICENSE.md for the full license.